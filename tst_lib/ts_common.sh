#!/bin/bash
# 测试套公共函数
# 约定：
#   1、以下划线"_"开头的函数和变量用例不能直接调用
#   2、环境变量全大写，全局变量加上"g_"前置，局部变量统一加"local"修饰

# source自己定义的其他公共函数
source "${TST_TS_TOPDIR}/tst_lib/other_common.sh" || exit 1

ts_setup() {
    msg "this is ts_setup"
    return 0
}

tc_setup_common() {
    msg "this is tc_setup_common"
    return 0
}

tc_teardown_common() {
    msg "this is tc_teardown_common"
    return 0
}

ts_teardown() {
    msg "this is ts_teardown"
    return 0
}
