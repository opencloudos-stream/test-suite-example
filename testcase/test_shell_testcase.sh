#!/bin/bash
###############################################################################
# @用例ID: 20220410-152859-724647329
# @用例名称: test_shell_testcase
# @用例级别: 3
# @用例标签:
# @用例类型: 功能
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpfile=test.file
g_tmpdir=./test_ls_cmd.tmpdir

tc_setup() {
    msg "this is tc_setup"
    # @预置条件: 支持ls命令
    which ls || return 1
    # @预置条件: 可以创建文件
    touch ./$g_tmpfile || return 1
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤:1: 新建临时目录
    mkdir $g_tmpdir || return 1

    # @测试步骤:2: 移动文件，ls检查文件
    # @预期结果:2: 文件移动成功，ls可以列出文件
    assert_true test -f ./$g_tmpfile

    assert_true mv -v ./$g_tmpfile $g_tmpdir

    assert_true test -f $g_tmpdir/$g_tmpfile

    return 0
}

tc_teardown() {
    msg "this is tc_teardown"

    rm -rfv ./$g_tmpfile $g_tmpdir || return 1

    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
